Where's the documentation?
===
If you're looking for documentation on fif, try checking...
- The built-in help documentation, by running `fif --help`
- [The usage section in the README file](https://gitlab.com/Lynnesbian/fif/-/blob/master/README.md#usage), for a 
  brief overview of fif's functionality and how to use it
- [The wiki](https://gitlab.com/Lynnesbian/fif/-/wikis/home), for more detailed information on fif's behaviour
- [Docs.rs](https://docs.rs/fif/latest/fif/), for information pertaining to fif's internals
