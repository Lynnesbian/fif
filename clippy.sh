#!/bin/bash

# SPDX-FileCopyrightText: 2021-2024 Lynnesbian
# SPDX-License-Identifier: CC0-1.0

set -e
source "$HOME"/.cargo/env || true

_extra=""
_ver=""
if [ "$1" == "ci" ]; then
  # deny on warnings when running in CI
 _extra="-Dwarnings"
elif [ "$1" == "nightly" ]; then
	_ver="+nightly"
fi

# allow find to fail
find . -name '*.rs' -exec touch "{}" \; || true

_backends=( "xdg-mime-backend" "infer-backend" )

for backend in "${_backends[@]}"; do
  cargo $_ver clippy --tests --features="$backend" -- \
   -W clippy::nursery \
   -W clippy::perf \
   -W clippy::pedantic \
   -W clippy::complexity \
   -W clippy::cargo \
   -W clippy::style \
   -W clippy::float_cmp_const \
   -W clippy::lossy_float_literal \
   -W clippy::multiple_inherent_impl \
   -W clippy::string_to_string \
   -A clippy::unused_io_amount \
   -A clippy::redundant_closure_for_method_calls \
   -A clippy::shadow_unrelated \
   -A clippy::option_if_let_else \
   -A clippy::multiple-crate-versions \
   -A clippy::must_use_candidate \
   -A clippy::missing_panics_doc \
   -A clippy::missing_errors_doc \
   -A clippy::doc_markdown \
   "$_extra"
done

# ALLOWS:
# unused_io_amount: there are two places where i want to read up to X bytes and i'm fine with getting less than that
# redundant_closure...: the alternative is often much more verbose
# shadow_unrelated: sometimes things that seem unrelated are actually related ;)
# option_if_let_else: the suggested code is usually harder to read than the original
# multiple_crate_versions: this doesn't actually trip right now, but it's not something i would want CI to fail over
# must_use_candidate: useless
# missing_panics_doc: the docs are just for me, fif isn't really intended to be used as a library, so this is unneeded
# missing_errors_doc: ditto
# doc_markdown: way too many false positives
