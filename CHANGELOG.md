# Changelog
Dates are given in YYYY-MM-DD format - for example, the 15th of October 2021 is written as 2021-10-15.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## v0.7.2 - 2024-08-07
### Changed
- Updated and pinned dependencies, using the latest MSRV-compatible versions available.
- Implement clippy suggestions

## v0.7.1 - 2024-01-24
### Changed
- Updated and pinned dependencies, using the latest MSRV-compatible versions available.

## v0.7.0 - 2023-03-15
### Changed
- The Minimum Supported Rust Version (MSRV) is now **1.64.0**.
- Update [`clap`] to v4.0
- Update [`infer`] to v0.13.0, bringing support for formats such as [CPIO](https://en.wikipedia.org/wiki/Cpio) and 
  [OpenRaster](https://www.openraster.org/index.html)

## v0.6.0 - 2022-09-04
### Changed
- The Minimum Supported Rust Version (MSRV) is now **1.57.0**.
- Update [`clap`] to v3.2.0
- Update [`smartstring`] to v1.0 - this should (slightly) improve performance on 32-bit big endian architectures 
  such as PowerPC

## v0.5.2 - 2022-05-02
### Added
- Output now contains a reminder to use `fif --fix`
### Changed
- Some extensions are considered to be always valid - these are:
  - "filepart", "part", "crdownload": Partially downloaded files, renaming could break download
  - "bak", "backup": Backup copies are a common idiom (e.g. "game.exe.bak") and should be respected
### Fixed
- Support for many file types that are subcategories of others (e.g., fif will no longer rename apk files to zip) (#1)

## v0.5.1 - 2022-04-12
### Added
- When using the [`infer`] backend, fif is now able to detect [Mach-O](https://en.wikipedia.org/wiki/Mach-O) binaries
### Changed
- Help output is now sorted manually, and flags are grouped by functionality
### Other
- (@hannesbraun) Updated [`infer`] to v0.6.0 (!2)
- Update [`clap`] to v3.1.0, fixing deprecated code
- Pin [`smartstring`] to a version that works on our MSRV

## v0.5.0 - 2022-01-01
### Changed
- The Minimum Supported Rust Version (MSRV) is now **1.57.0**.
- Updated [`new_mime_guess`] to 4.0.0
- `--version` output now handles missing Git commit hashes, and specifies the target operating system
### Fixed
- Disabled [`smartstring`] test on unsupported architectures
### Other
- Use [`parking_lot`]'s `RwLock` instead of the built-in one for a slight performance increase
- Added more command line argument tests

## v0.4.0 - 2021-10-14
### Added
- `--fix` mode - instead of outputting a shell script or text file, fif will rename the misnamed files for you!
  - By default, the user will be prompted only if fif encounters an error while renaming the file, or if renaming 
    the file would cause another file to be overwritten. This behaviour can be changed with the new `p`/`--prompt` 
    flag: `-p always` to be prompted each time, `-p error` to be prompted on errors and when a file would be 
    overwritten by renaming, and `-p never` to disable prompting altogether - this behaves the same as
    answering "yes" to every prompt.
  - The `--overwrite` flag must be specified along with `--fix` in order for fif to process renames that would cause an
    existing file to be overwritten. Without it, fif will never overwrite existing files, even with `-p always`.
    **Caution**: If this flag is set in combination with `--prompt never`, fif will overwrite files **without asking**!
  - For a more thorough breakdown of how these flags work, see [the corresponding wiki
    page](https://gitlab.com/Lynnesbian/fif/-/wikis/Fix).
### Changed
- The Minimum Supported Rust Version (MSRV) is now **1.48.0**.
- Capped help output (`-h`/`--help`) width at 120 characters max
- Output is now sorted by filename - specifically, errors will appear first, followed by files that fif is unable to 
  recommend an extension for, in order of filename, followed by files that fif knows how to rename, again in order 
  of filename.
### Other
- [Reuse](https://reuse.software) compliance

## v0.3.7 - 2021-09-25
### Added
- `-j`/`--jobs` flag for specifying the number of threads fif should use for scanning files
- AIFF (Audio Interchange File Format, a PCM audio format like WAV) detection to [`infer`] backend
- `--version` output now includes the (short) hash of the git commit fif was built from
### Changed
- fif will no longer use multithreading when scanning less than 32 files - the overhead of spawning threads isn't really
  worth it
### Other
- Refactoring - split fif into `main.rs` and `lib.rs`, moved file-related functionality (directory scanning, etc.) into
files module, removed string module, etc.
- Changelog "refactoring":
  - Based on the somewhat popular [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) guide, splitting the previous
    "Features" heading into "Added" and "Changed" sections, renaming "Bugfixes" to "Fixed", and removing the
    headings that (pointlessly?) previously divided the changelog into v0.3, v0.2, and v0.1
  - A few minor grammar tweaks and reorganisations
- Replaced [`cached`] dependency with a simple HashMap-backed store
- Replace all occurrences of [`once_cell`]'s `OnceCell` with equivalent `Lazy`-based implementations

## v0.3.6 - 2021-08-16
### Other
- Fixed another major dependency issue - [`clap`] version 3 beta **2** pulls in `clap_derive` version 3 beta **4**,
  causing fif to fail to build unless you use `--locked`. `Cargo.toml` now explicitly depends on `clap_derive` v3b2,
  which should fix this in (hopefully) all cases...
- Added an explicit dependency on `bitflags` 1.2.1, as 1.3+ requires Rust >= 1.46

## v0.3.5 - 2021-08-16 [YANKED]
### Other
- Updated `Cargo.toml` to pin [`clap`] to version 3 beta 2 - previously, version 3 beta 4 was considered compatible and
would be used if you ran `cargo install fif` instead of `cargo install --locked fif`, however, fif does *not* work with
this version of `clap`, which caused the build to fail. Also, `clap` 3 beta 4 depends on Rust >= 1.54, breaking our MSRV
- Fixed a few other `Cargo.toml` dependency versions that were causing issues when building on MSRV

## v0.3.4 - 2021-08-07
### Added
- `-I`/`--ignore-unknown-exts` flag for ignoring files with unknown extensions - for example, if fif doesn't know
  what a ".fake" file is, setting this flag will prevent it from renaming "photo.fake" to "photo.jpg". This is useful
  for avoiding the case where fif incorrectly mislabels an obscure format it isn't aware of as something else.
- (@herbygillot) Added [MacPorts](https://www.macports.org/) install instructions (!1)
### Other
- Refactored `formats.rs`
- More accurate dependency versions in `Cargo.toml` to ensure that the MSRV stays supported
- Sister project (?) [`new-mime-guess`] is now v3.0.0, updated to 2018 edition Rust, and with some new file and MIME
  types added

## v0.3.3 - 2021-07-07
### Added
- `--canonical-paths` flag for outputting canonical paths in output - for example,
  `mv /home/lynne/file.jpg /home/lynne/file.mp3` instead  of the default `mv file.jpg file.mp3`
### Other
- The `FIF_LOG` environment variable can now be used to set log level, in addition to `RUST_LOG`
- Log output now uses abbreviated level names: For example, `[D] Message` instead of `[DEBUG] Message`

## v0.3.2 - 2021-06-14
### Changed
- Nicer version output: `fif -V` reports "fif v0.3.2" (instead of just "fif 0.3.2" without the "v"), and `fif --version`
  reports `fif v0.3.2 (XDG-Mime backend)`, or whatever backend you're using
- fif's trace output now includes its version, backend, operating system, and architecture
### Fixed
- Fixed PowerShell output regression introduced in v0.2.13, whoops
### Other
- Block compilation if both the `xdg-mime-backend` and `infer-backend`
  [features](https://gitlab.com/Lynnesbian/fif/-/wikis/Cargo-Features) are enabled

## v0.3.1 - 2021-06-07
### Added
- JSON output support via `-o json`
- Plaintext output support via `-o text`
### Changed
- `videos` is now an alias for `video`
### Other
- More extensive README documentation

## v0.3.0 - 2021-04-28
### Added
- `-x`/`--exclude` flag for excluding file extensions (overrides `-e` or `-E` - `-E images -x jpg` scans all image
  files, except ".jpg" files)
- `-X`/`--exclude-set` flag for excluding sets of files, with the same syntax and sets as `-E`
- `-q`/`--quiet` flag for reducing output verbosity
### Changed
- The `videos` extension set has been renamed to `video`, in line with `audio`. `fif --help` has actually mistakenly
  referred to the set as `video` since v0.2.12! 0uo
- In addition to supplying included extensions as a comma separated list (like `-e jpg,png`), it is now possible to
  supply them through multiple uses of the `-e` flag (like `-e jpg -e png`). This also applies to `-x`
- `-e` and `-E` no longer conflict with each other, and can now be used together. For example, `-E images -e mp3`
  will scan all images *and* all MP3 files
- It is now possible to specify multiple extension sets at once: `-E images,system` will scan all images and archives
- fif's output now includes the directory it was run from
- Changed default verbosity to `info`
### Fixed
- Resolved some discrepancies between `application/xml` and `text/xml`
### Other
- Published my fork of ['mime_guess'] as ['new_mime_guess'], allowing it to be used properly with
  [crates.io](https://crates.io)
- CI has been vastly improved

## v0.2
### v0.2.13 (2021-04-26)
### Added
- `-v`/`--verbose` flag for setting verbosity without using `RUST_LOG`
- System extension set (`.dll`, `.so`, `.exe`...)
- [`infer`] backend now supports Ren'Py archive (`.rpa`) files
### Changed
- Output is now sorted: Files that couldn't be read, then files with no known MIME type, then files with no known
  extensions, then files with the wrong extension
- Added Apple iWork document formats to Documents extension set (`.pages`, `.key`, `.numbers`)
### Fixed
- Fixed some bad formatting in PowerShell output
- Always quote file paths in output, even when not necessary - This makes output more portable and less likely to break
  in future, or if [`snailquote`] misses something
### Other
- Cleaned up and properly documented tests
- Renamed `Script` (in `formats.rs`) to `Shell`, in line with renaming in `parameters.rs`
- [`xdg-mime`] no longer uses git version
- Output `\r\n` on Windows
- Use a macro to generate `Writable` arrays, making the code a little cleaner and nicer to write

### v0.2.12 (2021-04-14)
### Added
- Text extension set
- Better documentation for command line arguments
### Fixed
- Fixed a very minor output bug relating to scanning symlinked directories
- Better detection for pre-OOXML Office files
### Other
- Much better README.md
- Added more stuff to test.py
- PKGBUILD for Arch-based distros
- More test coverage
- Doubled BUF_SIZE

### v0.2.11 (2021-04-04)
### Added
- fif can now traverse symlinks with the `-f`/`--follow-symlinks` flag
### Changed
- Better MIME type detection:
  - Consider "some/x-thing" and "some/thing" to be identical
  - Use a patched version of mime_guess (which took a while to make 0u0;) with many more extension/type mappings
- Extensions are no longer mandatory - running fif without `-e` or `-E` will scan all files, regardless of extension
  (files without extensions are still skipped unless the -S flag is used)
### Fixed
- Fixed compilation on big endian 32-bit architectures (see
  [here](https://github.com/bodil/smartstring/blob/v0.2.7/src/config.rs#L102-L104) for why that was a problem in the
  first place)
- Fixed broken tests for the [`infer`] backend

### v0.2.10 (2021-03-26)
### Added
- PowerShell support!

## v0.2.9 - 2021-03-17
### Other
- Replaced a bunch of `PathBuf`s with `Path`s, which should reduce memory usage
- Formatting improvements

## v0.2.8 - 2021-03-03
### Added
- Much more information to help output: author, copyright, etc.
- Scan files without extensions with `-S` (by default, such files are ignored)
### Fixed
- Using `-s` to skip hidden files no longer skips all files if the root directory itself is hidden
### Other
- The `ScanError` enum now contains a `PathBuf` - Errors now return `ScanError` rather than `(ScanError, PathBuf)`
- Renamed modules in accordance with [Rust's API guidelines](https://rust-lang.github.io/api-guidelines/naming.html)

## v0.2.7 - 2021-03-01
### Added
- Documentation! And lots of it! 0u0
- Added a test for argument parsing
### Changed
- Default to `WARN`-level logging if `RUST_LOG` isn't set

### Other
- Drone CI config
- `test.py` for automated building and testing against Rust stable, beta, nightly, and the MSRV specified in
  `Cargo.toml`

## v0.2.6 - 2021-02-28
### Added
- Tests!
### Changed
- Default to [`xdg-mime`] on all Unixy platforms, not just Linux - this also includes the various *BSDs (I've tested
  FreeBSD), macOS (haven't tested, but I have a very old MacBook running Leopard that has `file` preinstalled, so it
  *should* work fine), Redox OS (haven't tested), etc.

## v0.2.5 - 2021-02-27
### Other
- Use [`xdg-mime`] by default on Linux, [`infer`] elsewhere

## v0.2.4 - 2021-02-22
### Added
- Proper(ish) XML document support
- Display version in help output

### v0.2.3+hotfix (2021-02-22)
### Added
- A quick hack to fix broken/non-existent support for XML document files - `docx`, `odt`, etc.

## v0.2.3 - 2021-02-22
### Added
- Automatically disable [`xdg-mime`] backend on Windows
- Exit codes
- Improved error handling
- Retrieve extension sets from [`mime_guess`] rather than hardcoding them
### Changed
- Switched back from `printf` to `echo` in shell output
### Fixed
- Improved SVG detection
### Other
- More frequent and detailed comments
- Refactored `formats.rs`
- Exclude certain files and directories from the crate

## v0.2.2 - 2021-02-20
### Added
- Windows support

## v0.2.1 - 2021-02-18
### Added
- Extension sets -- you can now use, for example, `-E images` to check files with known image extensions
- Shell script output now uses `printf` instead of `echo`
- [`infer`] backend, configurable with [Cargo features](https://gitlab.com/Lynnesbian/fif/-/wikis/Cargo-Features)
### Fixed
- Fixed broken singlethreaded support
### Other
- Use a global backend instance instead of passing `&db` around constantly
- Use `rustfmt` 0u0

## v0.2.0 - 2021-02-15
### Added
- Output a script rather than a list of misnamed files
- Parallel file scanning
- Logging support
### Fixed
- Handle filenames with invalid UTF-8
### Other
- Now licensed (under GPLv3)
- Replaced [`structopt`] with [`clap`] 3 (beta)
- Specify 1.43.0 as minimum supported Rust version

## v0.1.0 - 2021-02-04
Initial commit!
- Only one backend - [`xdg-mime`]
- No output formats - just prints a list of files to be renamed
- Only supported flags are `-e` (specify extensions) and `-s` (scan hidden files)

<!-- links -->
[`cached`]: https://crates.io/crates/cached
[`clap`]: https://crates.io/crates/clap
[`infer`]: https://crates.io/crates/infer
[`mime_guess`]: https://crates.io/crates/mime_guess
[`new_mime_guess`]: https://crates.io/crates/new_mime_guess
[`once_cell`]: https://crates.io/crates/once_cell
[`parking_lot`]: https://crates.io/crates/parking_lot
[`smartstring]: https://crates.io/crates/smartstring
[`snailquote`]: https://crates.io/crates/snailquote
[`structopt`]: https://crates.io/crates/structopt
[`xdg-mime`]: https://crates.io/crates/xdg-mime
