// SPDX-FileCopyrightText: 2021-2024 Lynnesbian
// SPDX-License-Identifier: GPL-3.0-or-later

use std::process::Command;

#[allow(unreachable_code, clippy::pedantic)]
fn main() -> Result<(), String> {
	#[cfg(all(feature = "infer-backend", feature = "xdg-mime-backend"))]
	// fail build if the user has set both the infer and xdg-mime backends
	return Err(String::from(
		"fif cannot be compiled with multiple backends set - please enable only one, or use the default.",
	));

	// a more robust way of doing this would be to use vergen (https://github.com/rustyhorde/vergen), but it pulls in a
	// whole bunch of extra dependencies (including chrono and git2), and also blocks compilation on the current MSRV.
	// this method is less clever and robust, but it works!
	let git = Command::new("git").args(["rev-parse", "--short", "HEAD"]).output();
	let hash = match git {
		Ok(output) => String::from_utf8_lossy(&output.stdout).into(),
		Err(_) => {
			// git not being present (or failing) shouldn't block compilation
			println!("cargo:warning=Failed to retrieve git commit hash");
			String::from("???")
		}
	};

	println!("cargo:rustc-env=GIT_SHA={}", hash);
	println!("cargo:rustc-rerun-if-changed=.git/HEAD");

	Ok(())
}
