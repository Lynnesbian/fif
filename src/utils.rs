// SPDX-FileCopyrightText: 2021-2024 Lynnesbian
// SPDX-License-Identifier: GPL-3.0-or-later

//! Various minor utilities.

use std::str::FromStr;

use cfg_if::cfg_if;
use mime::Mime;
use once_cell::sync::Lazy;

use crate::String;

/// The current version of fif, as defined in Cargo.toml.
pub const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");

cfg_if! {
	if #[cfg(any(all(unix, feature = "infer-backend"), all(not(unix), not(feature = "xdg-mime-backend"))))] {
		/// The backend being used; either "Infer" or "XDG-Mime".
		pub const BACKEND: &str = "Infer";
	} else {
		/// The backend being used; either "Infer" or "XDG-Mime".
		pub const BACKEND: &str = "XDG-Mime";
	}
}

/// The version defined in Cargo.toml, prefixed with a v (e.g. "v0.4.0")
pub(crate) static CLAP_VERSION: Lazy<String> = Lazy::new(|| String::from("v") + VERSION.unwrap_or("???"));

/// The version defined in Cargo.toml, prefixed with a v (e.g. "v0.4.0"), followed by the chosen backend and
/// abbreviated git commit hash in parentheses - For example, "v0.4.0 (XDG-Mime backend, commit #043e097)"
pub static CLAP_LONG_VERSION: Lazy<String> = Lazy::new(|| {
	// handle cases where GIT_SHA is set to an empty string
	let commit = match option_env!("GIT_SHA") {
		Some(hash) if !hash.trim().is_empty() => String::from("commit #") + hash,
		_ => String::from("unknown commit"),
	};

	format!("v{} ({}, {} backend, {})", VERSION.unwrap_or("???"), os_name(), BACKEND, commit).into()
});

/// A [`Mime`] representing the "application/zip" MIME type.
pub static APPLICATION_ZIP: Lazy<Mime> = Lazy::new(|| Mime::from_str("application/zip").unwrap());

/// Returns the name of the target operating system with proper casing, like "Windows" or "macOS".
#[allow(clippy::option_map_unit_fn)]
pub fn os_name() -> String {
	match std::env::consts::OS {
		// special cases: "ios" should not be capitalised into "Ios", for example
		"ios" => "iOS".into(),
		"macos" => "macOS".into(),
		"freebsd" => "FreeBSD".into(),
		"openbsd" => "OpenBSD".into(),
		"netbsd" => "NetBSD".into(),
		"vxworks" => "VxWorks".into(),
		os => {
			// generic case: return consts::OS with the first letter in uppercase ("linux" -> "Linux")
			let mut os_upper = String::from(os);
			os_upper.get_mut(0..1).map(|first| first.make_ascii_uppercase());
			os_upper
		}
	}
}
