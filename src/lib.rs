// SPDX-FileCopyrightText: 2021-2024 Lynnesbian
// SPDX-License-Identifier: GPL-3.0-or-later

//! This library consists of all of the things fif needs to run. It only exists as a library to separate code, and to
//! make documentation and testing a bit easier. I don't recommend using this as a library for your crate, as it may
//! have breaking changes without incrementing the major version - it's really only meant to be a place for fif's
//! internals to live.
//!
//! You can view [fif's README](https://gitlab.com/Lynnesbian/fif/-/blob/master/README.md#fif) to learn more.

#![forbid(unsafe_code)]
#![warn(
	trivial_casts,
	unused_lifetimes,
	unused_qualifications,
	missing_copy_implementations,
	unused_allocation
)]

pub mod files;
pub mod findings;
pub mod formats;
pub mod mime_db;
pub mod parameters;
pub mod utils;

use cfg_if::cfg_if;
use once_cell::sync::Lazy;

use crate::findings::Findings;
use crate::mime_db::MimeDb;

cfg_if! {
	if #[cfg(not(all(target_endian = "big", target_pointer_width = "32")))] {
		/// On most architectures, this is a type alias for [`SmartString`](crate). However, on [architectures
		/// unsupported by `smartstring`](https://github.com/bodil/smartstring/blob/v0.2.9/src/config.rs#L91-L93), this
		/// is simply an alias to [`std::string::String`].
		pub use smartstring::alias::String;
	} else {
		/// On most architectures, this is a type alias for [`SmartString`](crate). However, on [architectures
		/// unsupported by `smartstring`](https://github.com/bodil/smartstring/blob/v0.2.9/src/config.rs#L91-L93), this
		/// is simply an alias to [`std::string::String`].
		// one particular arch that this needs to be turned off for is powerpc (the 32 bit variant that the pre-G5
		// powerpc macs used)
		pub use std::string::String;
	}
}
cfg_if! {
	if #[cfg(any(all(unix, feature = "infer-backend"), all(not(unix), not(feature = "xdg-mime-backend"))))] {
		/// A [`Lazy`] holding an instance of [`mime_db::MimeDb`]. Initialised at program startup.
		pub static MIMEDB: Lazy<mime_db::InferDb> = Lazy::new(mime_db::InferDb::init);
	} else {
		/// A [`Lazy`] holding an instance of [`mime_db::MimeDb`]. Initialised at program startup.
		pub static MIMEDB: Lazy<mime_db::XdgDb> = Lazy::new(mime_db::XdgDb::init);
	}
}
